<?php
/**
 * Plugin Name:       Responsive Images
 * Plugin URI:        https://bitbucket.org/lewisdorigo/responsive-images
 * Description:       Adds ability for wordpress to generate image sizes on the fly
 * Version:           3.1.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo;

if(!defined('\ABSPATH')) {
    exit;
}

define('DRGO_IMAGES_PLUGIN', __FILE__);
define('DRGO_IMAGES_PLUGIN_DIR', __DIR__);

require_once(DRGO_IMAGES_PLUGIN_DIR.'/lib/Image.php');
require_once(DRGO_IMAGES_PLUGIN_DIR.'/lib/Media.php');


require_once(DRGO_IMAGES_PLUGIN_DIR.'/lib/Resize.php');

\Dorigo\Images\Resize::getInstance();