;(function($, w, d) {
    "use strict";


    $(d).ready(function() {

        console.log($, $('body'));

        $('body').on('click', '.drgo-image-focal-point__image', function(e) {
            e.preventDefault();

            var $this = $(this),
                $parent = $this.closest('.drgo-image-focal-point'),
                $marker = $('.drgo-image-focal-point__marker', $parent),
                $form = $this.closest('form.compat-item'),

                id = $parent.attr('data-id'),
                position = $this.offset(),

                height = this.offsetHeight,
                width = this.offsetWidth,

                clickX = e.pageX - position.left,
                clickY = e.pageY - position.top,

                x = (clickX / width) * 100,
                y = (clickY / height) * 100;

            var $xField = $('input[name="attachments['+id+'][drgo_focal_x]"]', $form),
                $yField = $('input[name="attachments['+id+'][drgo_focal_y]"]', $form);

            console.log({$xField, $yField});

            $marker.css({
                top: y+'%',
                left: x+'%',
            });

            $xField.val(x);
            $yField.val(y).change();

       });

    });
})(jQuery, window, document);