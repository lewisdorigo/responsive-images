# Wordpress Responsive Images

Various functions for generating image sizes on the fly with WordPress, and
generating `srcset` attributes from various image sizes.

Using Wordpress’s built in functions, you can generate new sizes of a featured
image:
    
    <?php get_the_post_thumbnail(null, [1280,480]); ?>

Or get an arbitrary image at specified size:
    
    <?php wp_get_attachment_image_src(12, [1280,580]); ?>

You can also get an arbitrary image at multiple sizes using the `\Dorigo\Image()`
class.

When creating an `\Dorigo\Image()` object, you pass through an image ID, or an
image object from Advanced Custom Fields.

You can then use the `sizes` function to get a sizes array. The first argument
is an array of target sizes.

The second is the (optional) aspect target aspect ratio of the image. This can
be left null to use the image’s original aspect ratio.

There is a third, optional, boolean argument for whether to size based on height
instead of width. When resizing based on height, the aspect ratio will apply to
the height.
    
    <?php
        $image = new \Dorigo\Image(12);
        $sizes = $image->sizes([1280, 640, 320], 0.4);
    ?>
 
And then generate a srcset from from images at arbitrary sizes:
    
    <?php
        $image = new \Dorigo\Image(12);
        $sizes = $image->sizes([1280, 640, 320], 0.4);
        
        $srcset = $image::srcset($sizes);
        $scaled_srcset = $image::srcset($sizes,true);
    ?>

----------

The plugins for generating `<img>` elements with `srcset` and `sizes` attributes:

1. `\Dorigo\Media::rem()`
1. `\Dorigo\Media::em()`
2. `\Dorigo\Media::maxWidth()`
3. `\Dorigo\Media::calc()`

These functions can be used in conjunction with the `\Dorigo\Images` functions
like so:

    <?php
        $image = new \Dorigo\Image(12);
        $sizes = $image->sizes([640, 480, 320, 320, 480, 192], 1);
    ?>
    
    <img src="<?= end($image_sizes)[0]; ?>" alt="" title=""
      srcset="<?= $image::srcset($sizes); ?>"
       sizes="(min-width: <?= \Dorigo\Media::maxWidth(); ?>) <?= \Dorigo\Media::rem(460); ?>,
              (min-width: <?= \Dorigo\Media::rem(768); ?>) <?= \Dorigo\Media::calc(195.72, 0.33333, 768, 0.9); ?>,
              <?= \Dorigo\Media::(280, 1, 320, 1); ?>">
    
Which will output something like this:

    <img src="/image-192x192.jpg" alt="" title=""
      srcset="/image-640x640.jpg 640w,
              /image-480x480.jpg 480w,
              /image-320x320.jpg 320w,
              /image-240x240.jpg 240w,
              /image-192x192.jpg 192w"
       sizes="(min-width: 87.5rem) 28.75rem,
              (min-width: 48rem) calc(29.9997vw - 2.14861rem),
              calc(100vw - 2.5rem)">
