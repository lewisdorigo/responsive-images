<?php

require('media.class.php');

class MediaTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        \Dorigo\Media::$fontSize       = 16;
        \Dorigo\Media::$maxWrapper     = 1400;
        \Dorigo\Media::$wrapperPercent = 1;

        \Dorigo\Media::setWidth('large',  1024);
        \Dorigo\Media::setWidth('medium', 768);
        \Dorigo\Media::setWidth('small',  480);
        \Dorigo\Media::setWidth('xsmall', 320);
    }

    protected function _after() {
    }

    public function testRems() {
        $this->assertEquals('1rem', \Dorigo\Media::rem(16));

        \Dorigo\Media::$fontSize = 12;

        $this->assertEquals('1.33333rem', \Dorigo\Media::rem(16));
    }

    public function testCalc() {
        $this->assertEquals('calc(50vw - 8rem)', \Dorigo\Media::calc(512, 0.5));

        \Dorigo\Media::$wrapperPercent = 0.9;

        $this->assertEquals('calc(45vw - 4rem)', \Dorigo\Media::calc(512, 0.5));

        \Dorigo\Media::$fontSize = 12;

        $this->assertEquals('calc(45vw - 5.33333rem)', \Dorigo\Media::calc(512, 0.5));
    }

    public function testContainerWidth() {
        $this->assertEquals('80rem', \Dorigo\Media::containerWidth(1280));
        $this->assertEquals(1280, \Dorigo\Media::containerWidth(1280,null,false));

        \Dorigo\Media::$wrapperPercent = 0.9;

        $this->assertEquals('72rem', \Dorigo\Media::containerWidth(1280));
        $this->assertEquals(1152, \Dorigo\Media::containerWidth(1280,null,false));

        \Dorigo\Media::$fontSize = 12;

        $this->assertEquals('96rem', \Dorigo\Media::containerWidth(1280));
        $this->assertEquals(1152, \Dorigo\Media::containerWidth(1280,null,false));
    }

    public function testMediaQuery() {
        $this->assertEquals('(min-width: 48em) and (max-width: 63.9375em)', \Dorigo\Media::mq('medium'));
        $this->assertEquals('(min-width: 48em)', \Dorigo\Media::mq(['min' => 'medium']));
        $this->assertEquals('(min-width: 20em) and (max-width: 31.25em)', \Dorigo\Media::mq(['min' => 'xsmall', 'max' => 500]));
    }
}