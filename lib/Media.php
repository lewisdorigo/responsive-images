<?php namespace Dorigo;

/**
 * Helper functions for use with the `srcset` and `sizes` attributes, and the
 * `picture` element.
 *
 * There’s also some definitions for media query sizes. These should match up
 * with the media query names defined in SASS.
 *
 * Can be used like so:
 *

   <img src="//placehold.it/128x128" alt="Placeholder" title="placeholder"
     srcset="//placehold.it/640x640 640w,
             //placehold.it/480x480 480w,
             //placehold.it/320x320 320w,
             //placehold.it/256x256 256w,
             //placehold.it/192x192 192w"
      sizes="(min-width: <?= /Dorigo/Media::max_width(); ?>) <?= /Dorigo/Media::rem(460); ?>,
             (min-width: <?= /Dorigo/Media::rem(768); ?>) <?= /Dorigo/Media::calc(195.72, 0.33333, 767, 0.9); ?>,
             <?= /Dorigo/Media::(280, 1, 320, 1); ?>">
 *
 * `/Dorigo/Media::rem()` will return the selected with in rems, given the base
 * font-size.
 *
 * `/Dorigo/Media::max_width()` will return the minimum browser width that will
 * result in the largest container size.
 *
 * `/Dorigo/Media::calc()` will return a css `calc()` value for the correct
 * size at any width, given the a browser window size size of the image at that
 * size, the main container width (as a percent of the browser width), and the
 * width of the image as a perent of the container.
 *
 * (You need to give the image size so it can work out column gutters at that
 * size)
 */

class Media {
    public static $fontSize       = 16;
    public static $maxWrapper     = 1400;
    public static $wrapperPercent = 1;

    private static $breakpoints = [];



    public static function rem($target) {
        return round($target / self::$fontSize, 5)."rem";
    }



    public static function em($target, $fontSize = null) {
        $fontSize = (float) $fontSize?:self::$fontSize;
        return round($target / $fontSize, 5)."em";
    }




    public static function maxWidth($container = null, $percent = null, $rems = true) {
        $maxWrapper     = (int)   $container ?: self::$maxWrapper;
        $wrapperPercent = (float) $percent   ?: self::$wrapperPercent;

        return $rems ? self::rem($maxWrapper / $wrapperPercent) : $maxWrapper / $wrapperPercent;
    }




    public static function calc($target, $containerPercent = 1, $viewportWidth = 1280, $wrapperPercent = false) {
        $wrapperPercent = (float) $wrapperPercent ?: self::$wrapperPercent;

        $vw = ($viewportWidth * $wrapperPercent * $containerPercent);

        return 'calc('.round(($vw/$viewportWidth) * 100, 5).'vw - '.self::rem($vw - $target).')';
    }




    public static function containerWidth($viewportWidth = 1280, $wrapperPercent = false, $rem = true) {
        $wrapperPercent = (float) $wrapperPercent ?: self::$wrapperPercent;

        return $rem ? self::rem($viewportWidth * $wrapperPercent) : $viewportWidth * $wrapperPercent;
    }




    public static function setWidth($name, $width, $sort = true) {
        self::$breakpoints[$name] = (int) $width;

        if($sort) { asort(self::$breakpoints); }
    }




    public static function getWidth($name, $rems = false) {
        $width = isset(self::$breakpoints[$name]) && self::$breakpoints[$name] ? self::$breakpoints[$name] : 0;
        return $rems ? self::rem($width) : $width;
    }




    public static function removeBreakpoint($name) {
        unset(self::$breakpoints[$name]);

        asort(self::$breakpoints);
    }




    public static function setBreakpoints($breakpoints) {
        foreach($breakpoints as $name => $width) {
            self::setWidth($name, $width, false);
        }

        asort(self::$breakpoints);
    }



    private static function breakpointSize($name, $sizes = 'both') {
        $breakpoints = [
            'min' => null,
            'max' => null,
        ];

        if(isset(self::$breakpoints[$name])) {
            $breakpoints['min'] = self::getWidth($name);

            if($sizes === 'min') {
                return $breakpoints['min'];
            }

            $names = array_keys(self::$breakpoints);
            $maxIndex = array_search($name, $names) + 1;

            if(count($names) > $maxIndex) {
                $maxName = $names[$maxIndex];
                $breakpoints['max'] = self::getWidth($maxName) - 1;
            }

            if($sizes === 'max') {
                return $breakpoints['max'];
            }
        }

        return $breakpoints;
    }

    public static function mq(...$queries) {
        $queryString = [];

        $count = 0;

        foreach($queries as $query) {
            $attribute  = 'width';
            $media      = null;
            $min        = null;
            $max        = null;
            $additional = '';

            if(is_numeric($query)) {
                $min = $query;
            } elseif(is_string($query)) {
                $query = self::breakpointSize($query);
                $min = $query['min'];
                $max = $query['max'];
            } elseif(is_array($query)) {
                $attribute  = isset($query['attribute'])  ? $query['attribute']  : $attribute;
                $media      = isset($query['media'])      ? $query['media']      : $media;
                $min        = isset($query['min'])        ? $query['min']        : $min;
                $max        = isset($query['max'])        ? $query['max']        : $max;
                $additional = isset($query['additional']) ? $query['additional'] : $additional;

                $min = is_string($min) ? self::breakpointSize($min, 'min') : $min;
                $max = is_string($max) ? self::breakpointSize($max, 'max') : $max;
            }

            $queryString[] = ($media ? "only {$media}" : '').
                             (($min || $max || $additional) && $media ? ' and ':'').
                             ($min ? "(min-{$attribute}: ".self::em($min).")" : '').
                             ($min && $max ? ' and ':'').
                             ($max ? "(max-{$attribute}: ".self::em($max).")" : '').
                             (($min || $max) && $additional ? ' and ' : '').
                             $additional;
        }

        return implode(', ', $queryString);
    }
}
