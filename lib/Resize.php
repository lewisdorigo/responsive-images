<?php namespace Dorigo\Images;

use Dorigo\Image;

class Resize {
    protected static $instance;

    protected function __construct() {
        add_filter('image_downsize', [$this, 'generate'], 9999, 3);
        add_action('admin_enqueue_scripts', [$this, 'loadScripts']);

        add_filter('attachment_fields_to_edit', [$this, 'addFields'], 10, 2);
        add_filter('attachment_fields_to_save', [$this, 'saveFields'], 10 , 2);
    }


    public function loadScripts() {
        wp_enqueue_script('drgo-images-focalpoint-js', plugins_url('/js/image-focal-point.js', DRGO_IMAGES_PLUGIN), ['jquery'], null, false);
        wp_enqueue_script('drgo-images-focalpoint-js');

        wp_enqueue_style('drgo-images-focalpoint-css', plugins_url('/css/image-focal-point.css', DRGO_IMAGES_PLUGIN));
    }

    public function addFields($fields, $post) {

        if('image' === substr( $post->post_mime_type, 0, 5 )) {

            $x = get_post_meta($post->ID, '_drgo_focal_x', true) ?: 50;
            $y = get_post_meta($post->ID, '_drgo_focal_y', true) ?: 50;

            $fields['drgo_focal_image'] = [
                'label' => __('Focal Point'),
                'input' => 'html',
                'html' => '<div class="drgo-image-focal-point" data-id="'.$post->ID.'">
                    <img src="'.wp_get_attachment_url( $post->ID ).'" alt="" title="" class="drgo-image-focal-point__image">
                    <div class="drgo-image-focal-point__marker" style="top:'.$y.'%;left:'.$x.'%;"></div>
                </div>',
                'helps' => __('Click on the image to set the focal point. If the image is cropped, it will crop towards that area.'),
            ];

            $fields['drgo_focal_x'] = [
                'label' => __('Focal Point (X)'),
                'input' => 'hidden', // this is default if "input" is omitted
                'value' => $x,
            ];

            $fields['drgo_focal_y'] = [
                'label' => __('Focal Point (Y)'),
                'input' => 'hidden', // this is default if "input" is omitted
                'value' => $y,
            ];
        }

        return $fields;
    }

    public function saveFields($post, $attachment) {

        $changed = false;

        $x = get_post_meta($post['ID'], '_drgo_focal_x') ?: 50;
        $y = get_post_meta($post['ID'], '_drgo_focal_y') ?: 50;

        $new_x = isset($attachment['drgo_focal_x']) && $attachment['drgo_focal_x'] ? $attachment['drgo_focal_x'] : 50;
        $new_y = isset($attachment['drgo_focal_y']) && $attachment['drgo_focal_y'] ? $attachment['drgo_focal_y'] : 50;

        if($new_x != $x) {
            $changed = true;
            update_post_meta($post['ID'], '_drgo_focal_x', $new_x);
        }
        if($new_y != $y) {
            $changed = true;
            update_post_meta($post['ID'], '_drgo_focal_y', $new_y);
        }

        if($changed) {
            $this->rebuildThumbnails($post['ID']);
        }

        return $post;

    }

    public static function getInstance() {
        if(is_null(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function rebuildThumbnails($post_id) {

        $backup_sizes = get_post_meta($post_id, '_wp_attackment_backup_sizes', true);
        $meta = wp_get_attachment_metadata($post_id);

        $sizes = array_merge($meta['sizes'], $backup_sizes);

        foreach($sizes as $key => $size_data) {
            $this->generate(false, $post_id, [
                $size_data['width'],
                $size_data['height'],
            ], $key, true);
        }

    }

    public static function generate($data, $id, $size, $key = null, $force = false) {
        if(!is_array($size) || !$id) { return false; }

        $meta = wp_get_attachment_metadata($id);
        if(!$meta) { return false; }

        $image = new Image($id);
        $id = $image->getId();

        $new_size = $image->dimensions($size[0], $size[1], true);
        $focus = $image->getImageFocalPoint();

        /**
         * If the new size is the image’s source size, return immediately.
         */
        if($new_size[0] == $meta['width'] && $new_size[1] == $meta['height']) {
            return false;
        }


        /**
         * If the image already exists at the new size, return that.
         */
        if(!apply_filters('Dorigo/Images/ForceRefresh', $force)) {
            foreach($meta['sizes'] as $_size => $size_data) {
                if($size_data['width'] == $new_size[0] && $size_data['height'] == $new_size[1]) {
                    return image_downsize($id, $_size);
                }
            }
        }

        $key = "resized_{$new_size[0]}x{$new_size[1]}";



        /**
         * Check if file with the name already exists. If the file exists, but
         * doesn’t exist in the image’s meta data, add the meta data.
         */
        $attached_file = get_attached_file($id);
        $path_info     = pathinfo($attached_file);

        $ext = $path_info['extension'];

        $upload_info   = wp_upload_dir();
        $upload_dir    = $upload_info['basedir'];

        $rel_path = str_replace([$upload_dir,".{$ext}"], '', $attached_file);
        $suffix   = "{$new_size[0]}x{$new_size[1]}";

        $dest_path = "{$rel_path}-{$suffix}.{$ext}";

        $resized_file = [
            'file'      => "{$path_info['filename']}-{$suffix}.{$ext}",
            'width'     => $new_size[0],
            'height'    => $new_size[1],
            'mime-type' => mime_content_type($attached_file)
        ];

        if(file_exists($upload_dir.DIRECTORY_SEPARATOR.$dest_path) && !isset($meta['sizes'][$key])) {

            $meta['sizes'][$key] = $resized_file;

            wp_update_attachment_metadata($id, $meta);

            $backup_sizes = get_post_meta($id, '_wp_attackment_backup_sizes', true);

            if(!is_array($backup_sizes)) { $backup_sizes = []; }

            $backup_sizes[$key] = $resized_file;
            update_post_meta($id, '_wp_attackment_backup_sizes', $backup_sizes);

            return false;
        }



        /**
         * Make the resized file, and update the metadata.
         */
        $cropSizes = $image->getCropDimensions([
            'width' => $new_size[0],
            'height' => $new_size[1],
            'ratio' => ($new_size[0] / $new_size[1]),
        ], $focus);

        $editor = wp_get_image_editor($attached_file);

        $file = $editor->crop(
            $cropSizes[0],
            $cropSizes[1],
            $cropSizes[2],
            $cropSizes[3],
            $new_size[0],
            $new_size[1],
            false
        );

        $result = $editor->save($upload_dir.$dest_path);

        if(!is_wp_error($result)) {
            $meta['sizes'][$key] = $resized_file;

            wp_update_attachment_metadata($id, $meta);

            $backup_sizes = get_post_meta($id, '_wp_attackment_backup_sizes', true);

            if(!is_array($backup_sizes)) { $backup_sizes = []; }

            $backup_sizes[$key] = $resized_file;
            update_post_meta($id, '_wp_attackment_backup_sizes', $backup_sizes);

        }

        return false;
    }
}