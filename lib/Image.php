<?php namespace Dorigo;

class Image {
    private $image;
    private $image_id;
    private $src_width;
    private $src_height;

    private $src_ratio;
    private $src_ratio_height;

    public function __construct($image) {
        if(is_array($image) && isset($image['ID'])) {
            $this->image = $image;
            $this->image_id = $image['ID'];
        } elseif(!is_array($image)) {
            $this->image = acf_get_attachment($image);
            $this->image_id = $image;
        }

        if(!$this->image) {
            throw new \Exception('Can’t get image');
        }

        $this->src_width  = $this->image['width'];
        $this->src_height = $this->image['height'];

        $this->src_ratio  = $this->src_height / $this->src_width;
        $this->src_ratio_height  = $this->src_width / $this->src_height;
    }

    public function __toString() {
        return $this->image['url'];
    }

    public function getId() {
        return $this->image_id;
    }

    public function sizes(array $sizes, $ratio = null, $width = null) {
        $image_sizes = [];
        $width = $width !== null ? $width : true;

        if(!$ratio) {
            $ratio = $width ? $this->src_ratio : $this->src_ratio_height;
        }

        $smallerThanSrc = false;

        foreach($sizes as $size) {
            $new_width = $width?$size:$size*$ratio;
            $new_height = $width?$size*$ratio:$size;

            $smallerThanSrc = ($width?$this->src_width:$this->src_height) >= $size ? true : $smallerThanSrc;

            $image = wp_get_attachment_image_src($this->image_id, [$new_width, $new_height]);
            $key   = "{$image[1]}x{$image[2]}";

            if(!array_key_exists($key, $image_sizes)) {
                $image_sizes[$key] = $image;
            }
        }

        if(!$smallerThanSrc) {
            $small_width  = $this->src_width * ($width ? 1 : $ratio);
            $small_height = $this->src_height * ($width ? $ratio : 1);

            $image = wp_get_attachment_image_src($this->image_id, [$new_width, $new_height]);
            $key   = "{$image[1]}x{$image[2]}";

            if(!array_key_exists($key, $image_sizes)) {
                $image_sizes[$key] = $image;
            }
        }

        return apply_filters('Dorigo/Image/Sizes',array_values($image_sizes), $this->image_id, $sizes, $ratio, $width);
    }

    public static function srcset(array $sizes, $scaled = null) {
        if(count($sizes) <= 1) {
            return apply_filters('Dorigo/Images/Srcset', "{$sizes[0][0]} {$sizes[0][1]}w", $sizes, $scaled);
        }

        $images = [];
        $srcset = [];
        $smallest_width = PHP_INT_MAX;

        foreach($sizes as $image) {
            if(!array_key_exists($image[1], $images)) {
                $smallest_width = min($image[1], $smallest_width);

                $images[$image[1]] = $image;
            }
        }

        if($scaled) {
            ksort($images, SORT_NUMERIC);
        } else {
            krsort($images, SORT_NUMERIC);
        }

        foreach($images as $image) {
            if($image[1] < 1) { continue; }

            $srcset[] = $image[0]." ".($scaled ? round($image[1]/$smallest_width,2).'x' : $image[1].'w');
        }

        return apply_filters('Dorigo/Images/Srcset', implode(', ',$srcset), $sizes, $scaled);
    }

    public function dimensions($new_width = null, $new_height = null) {
        if(!$this->src_width || !$this->src_height || (!$new_width && !$new_height)) {
            return [];
        }

        bcscale(10);

        $src_ratio = array(
            'width' => $this->src_ratio,
            'height' => $this->src_ratio_height
        );

        if(!$new_width) {
            $new_width = $new_height * $this->src_ratio_height;
        }else if(!$new_height) {
            $new_height = $new_width & $this->src_ratio;
        }

        $width = round((float) $new_width);
        $height = round((float) $new_height);

        $ratio = array(
            'width' => $width / $height,
            'height' => $height / $width
        );

        if($width > $this->src_width && $height <= $this->src_height) {

            /**
             * If requested width is larger than the source image, but height is
             * less than or equal to source image, make the requested width equal
             * the source, then recalculate the required height.
             */

            $width = $this->src_width;
            $height = $width * $ratio['height'];

        } else if($width <= $this->src_width && $height > $this->src_height) {

            /**
             * If requested height is larger than the source image, but width is
             * less than or equal to source image, make the requested height equal
             * the source, then recalculate the required width.
             */

            $height = $this->src_height;
            $width = $height * $ratio['width'];

        } else if($width > $this->src_width && $height > $this->src_height) {

            /**
             * If both the requested width and height are larger than than the
             * source:
             *
             * 1. If the aspect ratio of the requested image equals the source,
             *    make return the source image’s dimensions.
             * 2. If the requested image is square, make the width and height equal
             *    to the smallest source dimension.
             * 3. If the requested image is landscape, set the new width to be equal
             *    to the souce width, and recalculate the height. If that’s larger
             *    than the source height, set the height to source, and recalculate
             *    the width.
             * 4. If the requested image is portrait, set the new width to be equal
             *    to the souce height, and recalculate the width. If that’s larger
             *    than the source width, set the width to source, and recalculate
             *    the height.
             */

            if($ratio['width'] == $src_ratio['width']) {
                $width = $this->src_width;
                $height = $this->src_height;
            } else if($ratio['width'] == 1) {
                $width = min($this->src_width, $this->src_height);
                $height = $width;
            } else if($ratio['width'] > 1) {
                $width = $this->src_width;
                $height = $width * $ratio['height'];

                if($height > $this->src_height) {
                    $height = $this->src_height;
                    $width = $height * $ratio['width'];
                }
            } else if($ratio['width'] < 1) {
                $height = $this->src_height;
                $width = $height * $ratio['width'];

                if($width > $this->src_width) {
                    $width = $this->src_width;
                    $height = $width * $ratio['height'];
                }
            }
        }

        $width = (int) round($width);
        $height = (int) round($height);

        return array (
            $width,
            $height,
            $ratio['height']
        );
    }

    public function getCropDimensions($new_size, $focus) {
        // Gather all dimension
        $dimensions = ['x' => [], 'y' => []];
        $directions = ['x' => 'width', 'y' => 'height'];
        $sizes = ['x' => 'src_width', 'y' => 'src_height'];

        // Define the correction the image needs to keep the same ratio after the cropping has taken place
        $cropCorrection = [
            'x' => $new_size['ratio'] / $this->src_ratio_height,
            'y' => $this->src_ratio_height / $new_size['ratio']
        ];

        // Check all the cropping values
        foreach ($dimensions as $axis => $dimension) {

            $axis_size = $this->{$sizes[$axis]};

            // Get the center position
            $dimensions[$axis]['center'] = ($focus[$axis] / 100) * $axis_size;
            // Get the starting position and let's correct the crop ratio
            $dimensions[$axis]['start'] = $dimensions[$axis]['center'] - ($axis_size * ($cropCorrection[$axis] / 2));
            // Get the ending position and let's correct the crop ratio
            $dimensions[$axis]['end'] = $dimensions[$axis]['center'] + ($axis_size * ($cropCorrection[$axis] / 2));

            // Is the start position lower than 0? That's not possible so let's correct it
            if ($dimensions[$axis]['start'] < 0) {
                // Adjust the ending, but don't make it higher than the image itself
                $dimensions[$axis]['end'] = min($dimensions[$axis]['end'] - $dimensions[$axis]['start'], $axis_size);
                // Adjust the start, but don't make it lower than 0
                $dimensions[$axis]['start'] = max($dimensions[$axis]['start'] - $dimensions[$axis]['start'], 0);
            }

            // Is the start position higher than the total image size? That's not possible so let's correct it
            if ($dimensions[$axis]['end'] > $axis_size) {
                // Adjust the start, but don't make it lower than 0
                $dimensions[$axis]['start'] = max($dimensions[$axis]['start'] + $axis_size - $dimensions[$axis]['end'], 0);
                // Adjust the ending, but don't make it higher than the image itself
                $dimensions[$axis]['end'] = min($dimensions[$axis]['end'] + $axis_size - $dimensions[$axis]['end'], $this->{$sizes[$axis]});
            }
        }

        // Excecute the WordPress image crop function
        return [
            $dimensions['x']['start'],
            $dimensions['y']['start'],
            $dimensions['x']['end'] - $dimensions['x']['start'],
            $dimensions['y']['end'] - $dimensions['y']['start'],
        ];
    }

    public function getImageFocalPoint() {

        $x = get_post_meta($this->image_id, '_drgo_focal_x', true) ?: 50;
        $y = get_post_meta($this->image_id, '_drgo_focal_y', true) ?: 50;

        return [
            'x' => $x,
            'y' => $y
        ];

    }
}
